This mathematica notebooks contain the implementation of the opinion importance function examples in Chapter 3 of Information fusion for norm consensus in virtual communities.


* *extremist opinion importance function.nb* contains the formula of both the equitable and the positively biased extremist opinion importance function.


* *natural opinion importance function.nb* contains the interpolation that gives the formula of both the equitable and the positively biased natural opinion importance function.


The functions have their formulas considered in the spectrum [lb,ub] and are ploted considering the spectrum [1,5] to understand its shape. To get the formulas of the importance functions in a given spectrum [a,b], just change the values of lb and ub.


This mathematica notebooks are part of the final research project: Information fusion for norm consensus in virtual communities by Marc Serramià Amorós.